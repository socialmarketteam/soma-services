import express from "express"
import fs from "fs"
import { UploadedFile } from "express-fileupload";
import SomaFile, { FileModel } from "../models/File"
import moment = require("moment");

class FileController {
    static get router() {
        const router = express.Router();

        router.post('/images', (req,res,next) => {
            if(req.files && req.files.file) {
                var file:UploadedFile = req.files.file as UploadedFile;
                var fileName = moment.utc().format("YYYYMMDDHHmmss") + "-" + file.name;
                fs.appendFile(`./files/images/${fileName}`, file.data, (err) => {
                    if(err) return next(err);
                    var config = res.app.locals.config;
                    var url = `${config.baseUrl}/public/images/${fileName}`
                    var fileRecord = new SomaFile(fileName, file.name || "", file.mimetype || "", url)
                    FileModel.create(fileRecord, (err, doc) => {
                        if(err) return next(err);
                        res.status(200).json(doc)
                    })
                })
            }
        })

        return router
    }
}

export default FileController