import MetaData from "../models/MetaData"

class MetaDataController {
    static get router() {
        const router = require('express').Router();

        router.post('/', MetaDataController.handlePostMetaData)

        return router
    }

    static async handlePostMetaData(req,res,next) {
        var {metadata} = req.body;
        if(!metadata) return res.status(400).json({
            error: {
                message: `missing metadata field in request body`
            }
        });
        try {
            await MetaData.create(metadata)
        } catch (error) {
            res.app.locals.logger.error(`Error: handlePostMetaData:21 - ${JSON.stringify(error.response)}`)
            return next(error)
        }
        res.status(200).json({})
    }
}

export default MetaDataController