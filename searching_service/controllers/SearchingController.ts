import { NextFunction, Response, Request, response } from "express";
import MetaData from "../models/MetaData"
import _ from "lodash"
import ServicesController from "./ServicesController"
import querystring from "querystring"

class SearchingController {
    private app:any;
    constructor(expressApp:any) {
        this.app = expressApp;
    }

    handleSearchRequest(req:Request, res:Response, next:NextFunction) {
        const {s} = req.query;

        this.search(s).then(result => {
            res.status(200).json(result)
        })
        .catch(error => {
            this.app.locals.logger.error(`searchingError: ${JSON.stringify(error)}`)
            next(error)
        });
    }

    search(searchString:string) {
        return new Promise((resolve, reject) => {
            if(!searchString) return resolve({});
            MetaData.find({doc_text: {$regex: searchString, $options: 'i'}}, async (error, docs) => {
                if(error) return reject(error);
                var documentSources = {};
                docs.forEach((doc:any) => {
                    if(doc.service && doc.doc_db && doc.doc_id) {
                        var sources = documentSources[doc.service];
                        if(!sources) {
                            documentSources[doc.service] = [{
                                collection: doc.doc_collection,
                                id: doc.doc_id
                            }]
                        } else {
                            sources.push({
                                collection: doc.doc_collection,
                                id: doc.doc_id
                            })
                        }
                    }
                })
                var requests:any[] = [];
                var result = {};
                Object.keys(documentSources).forEach(serviceName => {
                    var service = new ServicesController(serviceName);
                    if(service.api) {
                        const sourceDocs = documentSources[serviceName];
                        if(sourceDocs && sourceDocs.length) {
                            var collections = {};
                            _.uniqBy(sourceDocs, "collection").forEach((doc:any) => {
                                collections[doc.collection] = sourceDocs.filter(sourceDoc => sourceDoc.collection == doc.collection).map(sourceDoc => sourceDoc.id);
                            });
                            Object.keys(collections).forEach(collection => {
                                var ids = collections[collection];
                                var filter = JSON.stringify({
                                    _id: {$in: ids}
                                })
                                var query = querystring.stringify({
                                    filter
                                })
                                var url = `/data/${collection}?${query}`;
                                requests.push({
                                    collection,
                                    api: service.api,
                                    url
                                })
                            })
                        }
                    }
                })
                for(var i=0;i<requests.length;i++) {
                    var {url,collection,api} = requests[i];
                    try {
                        var response = await api.get(url)
                        result[collection] = response.data;
                    } catch (error) {
                        console.log(error)
                    }
                    
                }
                resolve(result)
            })
        })
    }
}

export default SearchingController