import mongoose from "mongoose"
import autoIncrement from "mongoose-auto-increment"
import moment from "moment"

autoIncrement.initialize(mongoose.connection);

const metaDataSchema = new mongoose.Schema({
    _id: {type: Number, required: true},
    doc_text: {type: String, required: true},
    doc_id: {type: Number, required:true},
    doc_collection: {type: String, required:true},
    doc_db: {type: String, required: true},
    service: {type: String, required: true},
    created_at: {type: Date, default: moment.utc()},
    updated_at: {type: Date, default: moment.utc()},
    deleted_at: {type: Date, default: null}
});

metaDataSchema.plugin(autoIncrement.plugin, {model: "MetaData", field: "_id", startAt: 1, incrementBy: 1});

export default mongoose.model("MetaData", metaDataSchema, 'meta_data')