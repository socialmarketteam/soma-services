const fs = require("fs")
const winston = require("winston")

const logger = (config:any) => {
    if (!fs.existsSync(config["dir_path"])) {
        fs.mkdirSync(config["dir_path"])
    }
    
    return new winston.Logger({
        "transports": [
            new winston.transports.File({
                "level": "info",
                "filename": config["filename"],
                "handleExceptions": true,
                "json": false,
                "maxsize": 5 * 1024 * 1024, // 5MB
                "maxFiles": 5,
                "colorize": false
            }),
            new winston.transports.Console({
                "level": "info",
                "handleExceptions": true,
                "json": false,
                "colorize": true
            })
        ]
    })
}

export default logger
