import express, {Express} from "express"
import config from "./config"
import logger from "./utils/logger"
import morgan from "morgan"
import bodyParser from "body-parser"
import cors from "cors"
import mongoose from "mongoose"
import SearchingController from "./controllers/SearchingController"
import AccessController from "./controllers/AccessController"
import MetaDataController from "./controllers/MetaDataController"

var app = express();
app.locals.config = config;
app.locals.logger = logger(config.logger);

const searchingController = new SearchingController(app);

const connectionUri = `mongodb://${app.locals.config.mongo.username}:${app.locals.config.mongo.password}@${app.locals.config.mongo.host}${app.locals.config.mongo.port ? `:${app.locals.config.mongo.port}` : `` }/${app.locals.config.mongo.database}${app.locals.config.mongo.options && app.locals.config.mongo.options.length > 0 ? `?${app.locals.config.mongo.options.join('&')}` : ``}`;

mongoose.connect(connectionUri, {useNewUrlParser: true})

const db = mongoose.connection;
db.on('error', console.error.bind(console, "Connection error:"))

app.use(morgan("combined", {
    stream: {
        write: (message:string) => {
            app.locals.logger.info(message)
        }
    }
}));
app.use(cors(app.locals.config.cors));
app.use(bodyParser.json({limit: '50mb', type: 'application/json'}))
app.use(bodyParser.urlencoded({ extended: false }))

// route options
app.options("*", cors(app.locals.config.cors))

app.get('/ping', (req,res,next) => {
    res.json("ok")
})

app.get('/search', 
    AccessController.restrictApiKeys([config.apiKeys.webApp,config.apiKeys.services]),
    searchingController.handleSearchRequest.bind(searchingController)
)
app.use('/metadata',
    AccessController.restrictApiKeys([config.apiKeys.webApp,config.apiKeys.services]),
    MetaDataController.router
)

// handles all errors
app.use((err:any, req:any, res:any, next:any) => {
    if (!err.response || !err.response.data) {
        app.locals.logger.error(`unexpectedError: ${JSON.stringify(err)}`)
        return res.status(500).json({"error": err.message})
    }

    app.locals.logger.error(`unexpectedError: ${JSON.stringify(err.response)}`)

    if (err.response.data.error) {
        let errorResponse = err.response.data.error

        let statusCode = errorResponse.code || 500
        let errorMessage = errorResponse.message || "Unknown error"

        if (errorResponse.context && errorResponse.context.resource) {
            const errorReasons = errorResponse.context.resource
            if (errorReasons.length > 0) {
                statusCode = Math.min(errorReasons[0].code, 599)
                errorMessage = errorReasons[0].message
            }
        }

        res.status(statusCode).json({"error": errorMessage})
        return
    }

    return res.status(Math.min(err.response.status, 500) || 500).json({"error": err.response.data.error})
})

app.listen(config.port, () => {
    console.log(`Searching service is listening on port ${config.port}`)
})

export default app