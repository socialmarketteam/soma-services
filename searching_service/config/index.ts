import process from "process"
import services from "./services"

const mongo = {
    "host": process.env.DB_HOST || "localhost",
    "port": process.env.DB_PORT || "27017",
    "username": process.env.DB_USER || "sa",
    "password": process.env.DB_PASS || "hoan",
    "database": process.env.DB_NAME || "document_meta_data",
    "options": process.env.DB_OPTIONS || ["authSource=user_management_db","readPreference=primary","ssl=false"]
}

const config = {
    port: process.env.PORT || 3002,
    logger: {
        dir_path: process.env.LOG_DIR_PATH || "logs",
        filename: process.env.LOG_FILENAME || "logs/searching_service-v1.log"
    },
    apiKeys: {
        webApp: process.env.WEB_APPLICATION_KEY || "5SywDbxtDvpT49JVGv5Q36Vr4gmCDrEMVkvBR2XxkFDVecJX",
        services: "Gv5Q36Vr4gmCDrxtDvpT4EMVk5SywDb9JVvBR2XxkFDVecJX"
    },
    cors: {
        methods: ["GET", "POST", "PUT", "PATCH", "DELETE"],
        allowedHeaders: ["Content-Type", "Authorization", "X-Application-Key"],
        optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
    },
    apps: {
        client: {
            domain: process.env.CLIENT_DOMAIN || "http://localhost:1234"
        }
    },
    mongo,
    services
}

export default config