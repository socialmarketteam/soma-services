const nodemailer = require("nodemailer")

let sharedServices = null

class NodeMailer {

    static get shared() {
        return sharedServices
    }

    constructor() {
        this.transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: "socialmarket.cs@gmail.com",
                pass: "hoan1531"
            }
        })
        if(!this.sharedServices) sharedServices = this
    }

    sendEmail(mailOptions, app) {
        this.transporter.sendMail(mailOptions, (err, info) => {
            if(err) {
                app.locals.logger.error("Send mail error: " + JSON.stringify(err))
                return
            }

            app.locals.logger.info("Send mail success: " +  JSON.stringify(info))
        })
    }

    static createMailOptions(options) {
        return {
            from: 'SoMa <socialmarket.cs@gmail.com>',
            to: options.to.join(','),
            subject: options.subject,
            text: options.text,
            html: options.html
        }
    }
}

new NodeMailer()

module.exports = NodeMailer