const express = require("express")
const app = express();
const mongoose = require("mongoose")
const path = require("path")
var requireMain = require.main || {
    filename: ""
}
app.locals.appRoot = path.resolve(requireMain.filename.replace("\\app.js", ""))
app.locals.templateDir = `${requireMain.filename.replace("\\app.js", "")}\\templates`

app.locals.config = require("./config")
app.locals.logger = require("./utils/logger")(app.locals.config["logger"])

const bodyParser = require("body-parser")
const morgan = require("morgan")
const cors = require("cors")
const passport = require("passport")

const AccountController = require("./controllers/AccountController")
const AuthController = require("./controllers/AuthController")
const UserController = require("./controllers/UserController")
const DataController = require("./controllers/DataController")
const dataController = new DataController(app)

app.use(morgan("combined", {
    "stream": {
        "write": (message) => {
            app.locals.logger.info(message)
        }
    }
}))
app.use(cors(app.locals.config.cors))
app.use(bodyParser.json({limit: '50mb', type: 'application/json'}))
app.use(bodyParser.urlencoded({ extended: false }))

app.use(passport.initialize())
passport.use(AccountController.Passport.localStrategy(app.locals.config.identity))
passport.use(AccountController.Passport.jwtStrategy(app.locals.config.identity))

// route options
app.options("*", cors(app.locals.config.cors))
app.use('/account', AuthController.restrictApiKeys([app.locals.config.apiKeys.webApp]))
app.use('/user', AuthController.restrictApiKeys([app.locals.config.apiKeys.webApp]))
app.use('/data', dataController.router)

// routes
app.use('/account', AccountController.router())
app.use('/user', UserController.router)

// handle all errors
app.use((err, req, res, next) => {
    if(res.finished) return
    if (!err.response || !err.response.data) {
        return res.status(500).json({"error": err.message})
    }

    if (err.response.data.error) {
        let errorResponse = err.response.data.error

        let statusCode = errorResponse.code || 500
        let errorMessage = errorResponse.message || "Unknown error"

        if (errorResponse.context && errorResponse.context.resource) {
            const errorReasons = errorResponse.context.resource
            if (errorReasons.length > 0) {
                statusCode = Math.min(errorReasons[0].code, 599)
                errorMessage = errorReasons[0].message
            }
        }

        res.status(statusCode).json({"error": errorMessage})
        return
    }

    return res.status(Math.min(err.response.status, 500) || 500).json({"error": err.response.data.error})
})

let connectionUri = `mongodb://${app.locals.config.mongo.username}:${app.locals.config.mongo.password}@${app.locals.config.mongo.host}${app.locals.config.mongo.port ? `:${app.locals.config.mongo.port}` : `` }/${app.locals.config.mongo.database}${app.locals.config.mongo.options && app.locals.config.mongo.options.length > 0 ? `?${app.locals.config.mongo.options.join('&')}` : ``}`
    
mongoose.connect(connectionUri, { useNewUrlParser: true })

var db = mongoose.connection;

db.on('error', console.error.bind(console, "Connection error:"))

app.listen(app.locals.config.port, () => {
    console.log(`User management service is listening on port ${app.locals.config.port}`)
})

module.exports = app.locals