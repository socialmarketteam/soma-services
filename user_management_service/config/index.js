const process = require("process")


const mongo = {
    "host": process.env.DB_HOST || "localhost",
    "port": process.env.DB_PORT || "27017",
    "username": process.env.DB_USER || "sa",
    "password": process.env.DB_PASS || "hoan",
    "database": process.env.DB_NAME || "soma",
    "options": process.env.DB_OPTIONS || ["authSource=user_management_db","readPreference=primary","ssl=false"]
}

const services = require('./services')

const config = {
    "port": process.env.PORT || 3001,
    "logger": {
        "dir_path": process.env.LOG_DIR_PATH || "logs",
        "filename": process.env.LOG_FILENAME || "logs/user_management_service-v1.log"
    },
    "identity": {
        "jwtIssuer": process.env.JWT_ISSUER || "accounts.soma.io",
        "jwtSecret": process.env.JWT_SECRET || "verysecretivesecret",
        "passwordSecret": process.env.password_SECRET || "k7jBq6Pnz4zdqfTDPawSUf8A",
        "passwordSalt": process.env.password_SALT || "yY6pdoYImsusawn6j4yZqizC",
    },
    "apiKeys": {
        "webApp": process.env.WEB_APPLICATION_KEY || "5SywDbxtDvpT49JVGv5Q36Vr4gmCDrEMVkvBR2XxkFDVecJX",
        "services": "Gv5Q36Vr4gmCDrxtDvpT4EMVk5SywDb9JVvBR2XxkFDVecJX"
    },
    "cors": {
        "methods": ["GET", "POST", "PUT", "PATCH", "DELETE"],
        "allowedHeaders": ["Content-Type", "Authorization", "X-Application-Key"],
        "optionsSuccessStatus": 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
    },
    "apps": {
        "client": {
            "domain": process.env.CLIENT_DOMAIN || "http://localhost:1234"
        }
    },
    mongo,
    services
}



module.exports = config