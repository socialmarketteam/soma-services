
const config = {
    "status": {
        "ok": 200,
        "badRequest": 400,
        "unAuthorized": 401,
        "forbidden": 403,
        "notFound": 404,
        "internalServerError": 500
    }
}

module.exports = config