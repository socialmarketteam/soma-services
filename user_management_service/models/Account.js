const mongoose = require("mongoose")
const autoIncrement = require("mongoose-auto-increment")
const moment = require("moment")

autoIncrement.initialize(mongoose.connection)

const accountSchema = new mongoose.Schema({
    _id: {type: Number, required:true},
    username: {type: String, default: null},
    password: {type: String, default: null},
    reset_code: {type: String, default: null},
    user_id: [{type: Number, ref: 'User'}],
    created_at: {type: Date, default: moment.utc(Date.now())},
    updated_at: {type: Date, default: moment.utc(Date.now())},
    deleted_at: {type: Date, default: null}
})

accountSchema.plugin(autoIncrement.plugin, { model: 'Account', field: '_id', startAt: 1, incrementBy: 1 })

module.exports = mongoose.model('Account', accountSchema, 'accounts')