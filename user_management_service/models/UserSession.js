const mongoose = require("mongoose")
const autoIncrement = require("mongoose-auto-increment")
const moment = require("moment")

autoIncrement.initialize(mongoose.connection)

const userSessionSchema = new mongoose.Schema({
    _id: {type: Number, required:true},
    token: {type: String, required: true},
    user_id: [{type: Number, ref: 'User'}],
    is_valid: {type: Boolean, required: true, default: true},
    created_at: {type: Date, default: moment.utc(Date.now())},
    updated_at: {type: Date, default: moment.utc(Date.now())},
    deleted_at: {type: Date, default: null}
})

userSessionSchema.plugin(autoIncrement.plugin, { model: 'UserSession', field: '_id', startAt: 1, incrementBy: 1 })

module.exports = mongoose.model('UserSession', userSessionSchema, 'user_sessions')