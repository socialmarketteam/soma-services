const UserModel = require('./User')
const AccountModel = require('./Account')
const UserSessionModel = require('./UserSession')

module.exports = {
    users: UserModel,
    accounts: AccountModel,
    user_sessions: UserSessionModel
}