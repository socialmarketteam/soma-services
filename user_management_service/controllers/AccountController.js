const passport = require("passport")
const LocalStrategy = require("passport-local").Strategy
const passportJwt = require("passport-jwt")
const CryptoHashGenerator = require("./CryptoHashGenerator")
const stringValidator = require("../helpers/stringValidator")
const nodeMailer = require("../utils/nodemailer")
const fs = require("fs")

const UserController = require("./UserController")
const Account = require("../models/Account")
const api = require("../restfulApi")
const localConfig = require("../config")
const IdentityFramework = require("./IdentityFramework")
const UserSessionController = require("./UserSessionController")
const MetaDataController = require("./MetaDataController")

class AccountController {

    static router() {        
        const router = require("express").Router();

        router.post("/identify",
            AccountController.identifyUserByJwtSessionToken
        )
    
        router.post("/register", 
            AccountController.register,
            async (req,res,next) => {
                res.status(api.status.ok).json(Object.assign({}, res.locals))
                try {
                    await UserController.updateOne({_id: res.locals.user._id}, Object.assign({}, res.locals.user, { "accounts": [res.locals.account] }))
                }
                catch (error) {
                    res.app.locals.logger.error(`Fail to update account on user ${res.locals.user._id}.`)
                }
            }
        )
    
        router.post("/session", 
            AccountController.authenticateUsingAccount,
            IdentityFramework.JwtSession.generateJwtSession,
            async (req,res,next) => {
                if(!res.locals.session_token) return res.status(api.status.internalServerError).json({
                    "code": "GENERATE_TOKEN_ERROR",
                    "error": "Cannot generate session token"
                })
                res.status(api.status.ok).json(Object.assign({}, res.locals, {"session_id": undefined}))
                try {
                    await UserController.updateOne({_id: res.locals.user._id}, Object.assign({}, res.locals.user, {"sessions": [...res.locals.user.sessions, res.locals.session_id]}))
                } catch (error) {
                    res.app.locals.logger.debug(`Fail to update sessions and accounts on user ${res.locals.user._id}. Session token: ${res.locals.session_token}`)
                }
            }
        )

        router.delete("/session/:userId",
            AccountController.invalidateSession
        )
    
        router.post("/reset-password",
            (req,res,next) => {
                if(!req.body.code || req.body.code.toString().length === 0) {
                    return next()
                }
                if(!req.body.newPassword || req.body.newPassword.toString().length === 0) return res.status(api.status.badRequest).json({"error": "Missing new password"})
                AccountController.changePasswordUsingCode(req.body.code,req,res)
            },
            AccountController.resetPassword,
            (req,res,next) => {
                if(!res.locals.account || !res.locals.account.resetCode || res.locals.account.resetCode.trim().length === 0) {
                    res.app.locals.logger.error("Reset password error: Cannot find reset code")
                    return
                }

                let locals = require('../app')
                let templatePath = `${locals.templateDir}\\EmailTemplates\\ResetPasswordEmailTemplate.html`
    
                let template = fs.readFileSync(templatePath, { encoding: "utf-8" })
                template = template.replace("{host}", res.app.locals.config.apps["client"].domain)
                template = template.replace("{code}", res.locals.account.resetCode)
    
                let mailOptions = nodeMailer.createMailOptions({
                    to: [req.body.email],
                    subject: "Reset password",
                    text: `You have sent a reset password request. You can now change your password at ${res.app.locals.config.apps["client"].domain}/auth/reset-password?code=${res.locals.account.resetCode}`,
                    html: template
                })
    
                nodeMailer.shared.sendEmail(mailOptions, res.app)
            }
        )
    
        return router
    }

    static invalidateSession(req,res,next) {
        if(!req.params.userId) {
            return res.status(400).json({
                error: "Missing user payload"
            })
        }
        
        UserController.findOne({_id: req.params.userId}).populate("sessions")
            .then(user => {
                if(!user) {
                    return res.status(404).json({
                        error: "User Not Found"
                    })
                }
                let sessionIds = user.sessions.map(session => {
                    return session._doc._id
                })
                UserSessionController.updateMany({_id: {$in: sessionIds}}, {is_valid: false})
                .then(() => {
                    res.status(200).json({})
                })
                .catch(error => {
                    res.app.locals.logger.error(`invalidateSessionError_${JSON.stringify(error)}_sessions_${sessionIds}`)
                    res.status(500).json({
                        error: error
                    })
                })
            })
            .catch(next)
    }

    static identifyUserByJwtSessionToken(req,res,next) {
        if(!req.body.token) return res.status(400).json({
            error: `Missing token`
        })
        passport.authenticate("jwt", (error, user) => {            
            if(user) return res.status(200).json(user)
            next(error)
        })({
            headers: {
                authorization: req.body.token
            }
        },res,next)
    }

    /**
     * @param account instance of Account
     */
    static async add(account){
        if(!account) throw new Error("Account payload is undefined")
        let newAccount = new Account()
        newAccount = Object.assign({}, newAccount, account, {"_id": undefined})
        let existAccount = await Account.findOne({ username: newAccount.username })
        if(existAccount && newAccount.user_id.length > 0) {
            await UserController.deleteOne({ _id: account.user_id[0] })
            return { "isExist": true }
        }
        newAccount.password = CryptoHashGenerator.sha256Hash(newAccount.password, localConfig.identity.passwordSalt, localConfig.identity.passwordSecret)
        return Account.create(newAccount)
    }

    /**
     * 
     * @param {Object} filter 
     */
    static findOne(filter) {
        if(!filter) throw new Error("Filter is undefined")
        filter = Object.assign({}, filter, {"deleted_at": null})
        return Account.findOne(filter)
    }

    /**
     * 
     * @param {Object} filter 
     * @param {Object} account 
     */
    static updateOne(filter, account) {
        if(!filter || !account) throw new Error("filter or account payload is undefined")
        filter = Object.assign({}, filter, {"deleted_at": null})
        account = Object.assign({}, account, {"updated_at": Date.now()})
        return Account.updateOne(filter, account)
    }

    /**
     * 
     * @param {Object} account 
     */
    static deleteOne(account){
        if(!account) throw new Error("account is undefined")
        account = Object.assign({}, account, {"deleted_at": Date.now()})
        return Account.updateOne({_id:account._id}, account)
    }
    
    /**
     * 
     * @param {Object} req 
     * @param {Object} res 
     * @param {Function} next
     */
    static authenticateUsingAccount(req,res,next) {
        passport.authenticate("local", (err, data, info) => {
            res.app.locals.logger.debug(info)
            if(err) return res.status(api.status.internalServerError).json(err)
            if(!data || !data.account || !data.user) return res.status(api.status.unAuthorized).json({"error": "Wrong username or password"})
            res.locals = Object.assign({}, res.locals, {"user": data.user, "account": Object.assign({}, data.account, {"password": undefined})})
            next()
        })(req,res,next)
    }

    /**
     * 
     * @param {Object} req 
     * @param {Object} res 
     * @param {Function} next 
     */
    static async register(req,res,next){
        try {
            if(!req.body.username || req.body.username.length === 0 || !req.body.password || req.body.password.length === 0) return res.status(api.status.badRequest).json({"error": `Username and password is required`})
            if((req.body.email && req.body.email.length > 0) || (req.body.phone && req.body.phone.length > 0)) {
                if(!stringValidator.isValidEmail(req.body.email)) return res.status(api.status.badRequest).json({"error": "Invalid email"})
                let filter = {}
                if(req.body.email && req.body.email.length > 0) filter = Object.assign({}, filter, { "email": req.body.email })
                if(req.body.phone && req.body.phone.length > 0) filter = Object.assign({}, filter, { "phone": req.body.phone })
                let existingUser = await UserController.findOne(filter)
                if(existingUser) {
                    return res.status(api.status.badRequest).json({"error": `${req.body.email === existingUser.email ? `User with email ${req.body.email} existing` : `User with phone ${req.body.phone} existing`}. Please choose another`})
                }
            }
            let newUser = await UserController.add(req.body)
            res.locals = Object.assign({}, res.locals, {"user": newUser._doc})
            req.body = Object.assign({}, req.body, { "user_id": [newUser._id] })
            let newAccount = await AccountController.add(req.body)
            if(newAccount.isExist) {
                return res.status(api.status.badRequest).json({"error": `Account with username ${req.body.username} is exist. Please choose another one`})
            }
            res.locals = Object.assign({}, res.locals, { "account": Object.assign({}, newAccount._doc, {"password": undefined}) })

            try {
                let metadata = {
                    doc_text: `${newUser.name || ""} ${newUser.email || ""} ${newUser.phone || ""}`,
                    doc_id: newUser._id,
                    doc_collection: "users",
                    doc_db: "soma",
                    service: "user_management_service"
                }
                await MetaDataController.createMetaData(metadata)
            } catch (error) {
                res.app.locals.error(`createMetaDataError: ${JSON.stringify(error.response)}`)
            }

            next()
        }
        catch(error) {
            if(res.locals.user) {
                await UserController.deleteOne(res.locals.user)
            }
            if(res.locals.account) {
                await AccountController.deleteOne(res.locals.account)
            }
            res.app.locals.logger.error(`register error at ${Date.now()}: ${JSON.stringify(error)}`)
            res.status(500).json({"error": error})
        }
    }

    /**
     * 
     * @param {Object} req 
     * @param {Object} res 
     * @param {Function} next 
     */
    static async resetPassword(req,res,next) {
        if(!req.body.email || req.body.email.trim().length === 0) {
            res.status(api.status.badRequest).json({"error": "Email cannot be empty."})
            return
        }
        if(!stringValidator.isValidEmail(req.body.email)) return res.status(api.status.badRequest).json({"error": "Invalid email"})
        try {
            let userProfile = await UserController.findOne({email: req.body.email})
            if(!userProfile) return res.status(api.status.badRequest).json({"error" : `Account with email ${req.body.email} not found.`})
            let account = await AccountController.findOne({user_id: { $in: [userProfile._id] }})
            if(!account) return res.status(api.status.badRequest).json({"error" : `Account with email ${req.body.email} not found.`})
            let resetCode = CryptoHashGenerator.random(res.app.locals.config.identity.passwordSecret)
            await AccountController.updateOne({_id: account._id}, Object.assign({}, account._doc, {"reset_code": resetCode}))
            res.locals = Object.assign({}, res.locals, {"account" : { "resetCode": resetCode }})
            res.status(api.status.ok).json({"resetCode": resetCode})
            next()
        }
        catch (error) {
            res.status(api.status.internalServerError).json({"error": error})
            res.app.locals.logger.error(`resetPassword error at ${Date.now()}: ${JSON.stringify(error)}`)
        }
    }

    /**
     * 
     * @param {String} code 
     * @param {Object} req 
     * @param {Object} res 
     */
    static async changePasswordUsingCode(code,req,res) {
        try {
            let account = await AccountController.findOne({reset_code: code})
            if(!account) return res.json(api.status.badRequest).json({"error": "Account not found"})
            let password = CryptoHashGenerator.sha256Hash(
                                                req.body.newPassword,
                                                res.app.locals.config.identity.passwordSalt,
                                                res.app.locals.config.identity.passwordSecret
                                            )
            account = Object.assign({}, account._doc, { "reset_code": null, "password": password })
            await AccountController.updateOne({_id: account._id}, account)
            return res.status(api.status.ok).json(Object.assign({}, account, { "password": undefined, "user_id": undefined }))
        } catch (error) {
            res.status(api.status.internalServerError).json({"error": error})
            res.app.locals.logger.error(`changePasswordUsingCode error at ${Date.now()}: ${JSON.stringify(error)}`)
        }
    }
}

AccountController.Passport = {
    "localStrategy": (config) => {
        return new LocalStrategy({
            "usernameField": "username",
            "passwordField": "password",
            "session": false
        }, (username, password, done) => {
            const hash = CryptoHashGenerator.sha256Hash(password, config.passwordSalt, config.passwordSecret)
            AccountController.findOne({username: username, password: hash, deleted_at: null}).populate('user_id')
                .then(account => {
                    if(!account || !account.user_id[0]._doc) return done(null, null)
                    done(null, Object.assign({}, {"account": account._doc, "user": account.user_id[0]._doc}))
                })
                .catch(error => {
                    done(error, null)
                })
        })
    },

    "jwtStrategy": (config) => {
        return new passportJwt.Strategy({
            "issuer": config.jwtIssuer,
            "secretOrKey": config.jwtSecret,
            "jwtFromRequest": passportJwt.ExtractJwt.fromAuthHeaderWithScheme("Bearer"),
            "passReqToCallback": true
        }, (req, jwtPayload, done) => {
            if (jwtPayload && jwtPayload._id !== undefined && jwtPayload !== null) {
                return done(null, jwtPayload)
            }
            return done(new Error("Invalid session."), null)
        })
    }
}

module.exports = AccountController