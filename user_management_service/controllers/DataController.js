const AuthController = require("./AuthController")
const collections = require("../models")

class DataController {
    constructor(app) {
        this.app = app
    }

    get router() {
        const router = require("express").Router()

        router.all('/:collection/:id?',
            AuthController.restrictApiKeys([this.app.locals.config.apiKeys.webApp,this.app.locals.config.apiKeys.services]),
            this.handleRequest
        )

        return router
    }

    handleRequest(req,res,next) {
        const {collection, id} = req.params
        if(!collection) res.status(400).json({
            error: "Missing collection"
        })
        const model = collections[collection]
        if(!model) res.status(400).json({
            error: "Collection not found"
        })

        let query = req.query
        let filter = query.filter ? JSON.parse(query.filter) : {}
        let payload = req.body
        let promise = undefined

        switch(req.method) {
            case "GET":
                let basic = id ? model.findById(id) : model.find(filter)
                promise = req.query.populate ? basic.populate(req.query.populate) : basic
            break;
            case "POST":
                promise = model.create(payload)
            break;
            case "PATCH":
                promise = model.findByIdAndUpdate(id || payload._id, payload)
            break;
            case "PUT":
                filter = filter !== {} ? filter : {}
                promise = model.updateMany(filter, payload)
            break;
            case "DELETE":
            break;
        }

        if(!promise) res.status(400).json({
            error: "Bad Request"
        })

        promise.then(response => {
            res.locals.data = response
            res.status(200).json(response)
        })
        .catch(error => {
            res.app.locals.logger.error(`dataQueryError_${JSON.stringify(error.response ? error.response.data : (error.message || error))}`)
            res.status(500).json({
                error
            })
        })
    }
}

module.exports = DataController