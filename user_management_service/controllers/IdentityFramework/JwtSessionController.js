const jwt = require("jsonwebtoken")
const api = require("../../restfulApi/index")
const UserSession = require("../../models/UserSession")

class JwtSessionController {

    static generateJwtSession(req,res,next) {
        if (!res.locals.user) {
            return res.status().json({
                "error": "Invalid JWT payload with key 'user'."
            })
        }

        const token = jwt.sign(Object.assign({}, res.locals.user, { "session_valid_from": Date.now() }), res.app.locals.config.identity.jwtSecret, {
            "issuer": res.app.locals.config.identity.jwtIssuer,
        })

        const session = new UserSession({token: token, user_id: [res.locals.user._id]})
        UserSession.create(session)
            .then(session => {
                res.locals = Object.assign({}, res.locals, {"session_token": session.token, "session_id": session._id})
                res.app.locals.logger.debug("trackSession: ", session.token)
                next()
            })
            .catch(error => {
                res.app.locals.logger.debug("trackSession error: ", error)
                next()
            })
    }

}

module.exports = JwtSessionController