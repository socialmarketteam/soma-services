const axios = require('axios')
const config = require('../config')

var api = {}

class MetaDataController {
    constructor() {
        api = axios.create({
            "baseURL": config.services.search_engine.baseUrl,
            "timeout": 60000,
            "headers": {
                "X-Application-Key": config.services.search_engine.apiKey
            }
        })
    }
    static async createMetaData(metadata) {
        if(!metadata) return;
        metadata.service = metadata.service || "user_management_service";
        await api.post('/metadata', {
            metadata
        })        
    }
}
new MetaDataController;
module.exports = MetaDataController