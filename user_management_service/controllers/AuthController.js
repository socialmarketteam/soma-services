

class AuthController {
    static restrictApiKeys(apiKeys){
        return (req, res, next) => {
            if (!req.headers["x-application-key"]) {
                return next(new Error("Missing X-Application-Key header."))
            }
            if (apiKeys.indexOf(req.headers["x-application-key"]) === -1)  {
                return next(new Error("Invalid X-Application-Key header."))
            }
            next()
        }
    }
}

module.exports = AuthController