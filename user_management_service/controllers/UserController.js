const User = require("../models/User")

class UserController {
    static add(user) {
        if (!user) throw new Error("User is undefined")
        let newUser = new User()
        newUser = Object.assign({}, newUser, user, { "_id": undefined })
        return User.create(newUser)
    }

    static find(filter) {
        if (!filter) throw new Error("Filter is undefined")
        filter = Object.assign({}, filter, { "deleted_at": null })
        return User.find(filter)
    }

    static findOne(filter) {
        if (!filter) throw new Error("Filter is undefined")
        filter = Object.assign({}, filter, { "deleted_at": null })
        return User.findOne(filter)
    }

    static updateOne(filter, user) {
        if (!filter || !user) throw new Error("filter or user payload is undefined")
        filter = Object.assign({}, filter, { "deleted_at": null })
        user = Object.assign({}, user, { "updated_at": Date.now() })
        return User.updateOne(filter, user)
    }

    static deleteOne(user) {
        if (!user) throw new Error("User is undefined")
        user = Object.assign({}, user, { "deleted_at": Date.now() })
        return User.updateOne({ _id: user._id }, user)
    }

    static async handleFriendRequest(req, res, next) {
        try {
            const { fromId, toId } = req.body
            if (!fromId || !toId) return res.status(400).json({
                error: "Missing required payload"
            });
            const fromUser = await UserController.findOne({ _id: fromId })
            const toUser = await UserController.findOne({ _id: toId })
            if (!fromUser || !toUser) return res.status(404).json({
                error: "Cannot find specified users"
            });
            fromUser._doc.sent_friend_requests.push(toId)
            toUser._doc.friend_requests.push(fromId)
            await UserController.updateOne({ _id: fromId }, fromUser._doc)
            await UserController.updateOne({ _id: toId }, toUser._doc)
            res.status(200).json({
                success: true,
                from: fromUser._doc,
                to: toUser._doc
            })
        } catch (error) {
            next(error)
        }
    }

    static async deleteFriendRequest(fromId, toId) {

        try {
            const fromUser = await UserController.findOne({ _id: parseInt(fromId) })
            const toUser = await UserController.findOne({ _id: parseInt(toId) })
            if (!fromUser || !toUser) return {
                error: "Cannot find specified users",
                success: false
            };
            fromUser._doc.sent_friend_requests = fromUser._doc.sent_friend_requests.filter(id => id !== toUser._id)
            toUser._doc.friend_requests = toUser._doc.friend_requests.filter(id => id !== fromUser._id)
            await UserController.updateOne({ _id: fromUser._id }, fromUser._doc)
            await UserController.updateOne({ _id: toUser._id }, toUser._doc)
            return {
                success: true,
                data: {
                    fromUser,
                    toUser
                }
            }
        } catch (error) {
            return {
                error,
                success: false
            }
        }
    }

    static async handleDeleteFriendRequest(req, res, next) {
        const { fromId, toId } = req.query
        if (!fromId || !toId) return res.status(400).json({
            error: "Missing required payload"
        });

        let deleteFriendRequestResult = await UserController.deleteFriendRequest(fromId, toId)

        if (deleteFriendRequestResult.error) return res.status(500).json(deleteFriendRequestResult.error)

        let { fromUser, toUser } = deleteFriendRequestResult.data

        res.status(200).json({
            success: true,
            from: fromUser._doc,
            to: toUser._doc
        })
    }

    static async acceptFriendRequest(fromId, toId) {
        try {
            const fromUser = await UserController.findOne({ _id: parseInt(fromId) })
            const toUser = await UserController.findOne({ _id: parseInt(toId) })
            if (!fromUser || !toUser) return {
                error: "Cannot find specified users",
                success: false
            };
            fromUser._doc.friends = [...fromUser._doc.friends.filter(friendId => friendId !== toUser._id), toUser._id]
            toUser._doc.friends = [...toUser._doc.friends.filter(friendId => friendId !== fromUser._id), fromUser._id]
            await UserController.updateOne({ _id: fromUser._id }, fromUser._doc)
            await UserController.updateOne({ _id: toUser._id }, toUser._doc)
            return {
                success: true,
                data: {
                    fromUser,
                    toUser
                }
            }
        } catch (error) {
            return {
                error,
                success: false
            }
        }
    }

    static async handleAcceptFriendRequest(req, res, next) {
        const { fromId, toId } = req.body
        if (!fromId || !toId) return res.status(400).json({
            error: "Missing required payload"
        });

        let acceptFriendRequestResult = await UserController.acceptFriendRequest(fromId, toId)

        if (acceptFriendRequestResult.error) return res.status(500).json(acceptFriendRequestResult.error)

        let deleteFriendRequestResult = await UserController.deleteFriendRequest(fromId, toId)

        if (deleteFriendRequestResult.error) return res.status(500).json(deleteFriendRequestResult.error)

        let { fromUser, toUser } = deleteFriendRequestResult.data

        res.status(200).json({
            success: true,
            from: fromUser._doc,
            to: toUser._doc
        })
    }

    static async deleteFriend(userId, friendId) {
        try {
            const user = await UserController.findOne({ _id: parseInt(userId) })
            const friend = await UserController.findOne({ _id: parseInt(friendId) })

            if (!user || !friend) return {
                error: "Cannot find specified users",
                success: false
            };

            user._doc.friends = user._doc.friends.filter(id => id !== friend._id)
            friend._doc.friends = friend._doc.friends.filter(id => id !== user._id)

            await UserController.updateOne({ _id: user._id }, user._doc)
            await UserController.updateOne({ _id: friend._id }, friend._doc)

            return {
                success: true
            }
        } catch (error) {
            return {
                error,
                success: false
            }
        }
    }

    static async handleDeleteFriend(req, res, next) {
        const { userId, friendId } = req.query

        if (!userId || !friendId) return res.status(400).json({
            error: "Missing required payload"
        });

        let deleteFriendResult = await UserController.deleteFriend(userId, friendId)

        if(deleteFriendResult.error) return res.status(500).json(deleteFriendResult.error)

        res.status(200).json({
            success: true
        })
    }

    static get router() {
        const router = require("express").Router();

        router.post('/friend_request', this.handleFriendRequest)

        router.delete('/friend_request', this.handleDeleteFriendRequest)

        router.post('/friend_request/accept', this.handleAcceptFriendRequest)

        router.delete('/friends', this.handleDeleteFriend)

        return router
    }
}

module.exports = UserController