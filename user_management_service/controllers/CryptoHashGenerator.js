
const CryptoJS = require("crypto-js")

module.exports = {
    "random": (secret) => {
        return CryptoJS.enc.Hex.stringify(CryptoJS.SHA256(`${new Date().getMilliseconds()}`, secret))
    },

    /**
     * @param {String} message
     * @param {String} salt
     * @param {String} secret
     */
    "sha256Hash": (message, salt = "", secret = "") => {
        const saltedChallenge = `${salt}.${message}`
        const hash = CryptoJS.HmacSHA256(saltedChallenge, secret)
        return CryptoJS.enc.Hex.stringify(hash)
    }
}
