const UserSession = require("../models/UserSession")

class UserSessionController {
    static deleteMany(condition) {
        if(!condition) throw new Error("Condition is undefined")
        return UserSession.deleteMany(condition)
    }
    static updateMany(condition, payload) {
        if(!condition) throw new Error("Condition is undefined")
        if(!payload) throw new Error("Payload is undefined")
        return UserSession.updateMany(condition, payload)
    }
}

module.exports = UserSessionController