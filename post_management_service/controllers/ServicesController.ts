import axios, { AxiosInstance } from "axios"

class ServicesController {
    public api:AxiosInstance;
    constructor(serviceName) {
        const config = require('../config').default
        const serviceApiConfig = config.services[serviceName];
        if(serviceApiConfig) {
            this.api = axios.create({
                baseURL: serviceApiConfig.baseURL,
                timeout: 60000,
                headers: {
                    "X-Application-Key": serviceApiConfig.apiKey
                }
            })
        }
    }
}

export default ServicesController