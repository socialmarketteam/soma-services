import Post, { PostModel } from "../models/Post"
import MetaDataController from "./MetaDataController"
import ServicesController from "./ServicesController";
import querystring from "querystring"
import { shuffle } from "../utils/array";
import PostComment, { PostCommentModel } from "../models/PostComments";

class PostController {
    static get router() {
        const router = require("express").Router();

        router.post('/', PostController.handleCreatePost)

        router.get('/newsfeed', PostController.fetchNewsFeed)

        router.get('/timeline', PostController.getTimeLinePosts)

        router.post('/like/:postId', PostController.likePost)
        
        router.post('/dislike/:postId', PostController.dislikePost)

        router.post('/comment/:postId', PostController.comment)

        return router;
    }

    static loadPost(filter, callback) {
        PostModel.find(filter).sort({created_at: 'desc'}).populate(['creator','likes','shares','files','comments']).limit(100).exec((err, docs) => {
            if(err) return callback({
                error: err
            })
            PostCommentModel.populate(docs, [{
                path: 'comments.creator',
                model: 'User'
            },{
                path: 'comments.file',
                model: 'File'
            }]).then((docs) => {
                // shuffle(docs)
                callback(null, docs)
            })
            .catch(error => {
                return callback({
                    error
                })
            })
        })
    }

    static getTimeLinePosts(req,res,next) {
        const {userId} = req.query;
        if(!userId) return res.status(400).json({
            error: "Cannot identify user"
        })
        PostModel.find({creator: userId}).sort({created_at: 'desc'}).populate(['creator','likes','shares','files','comments']).exec((err, docs) => {
            if(err) return res.status(500).json({error: err.message})
            PostCommentModel.populate(docs, [{
                path: 'comments.creator',
                model: 'User'
            },{
                path: 'comments.file',
                model: 'File'
            }]).then((docs) => {
                console.log(docs)
                res.status(200).json(docs)
            })
            .catch(next)
        })
    }

    static comment(req,res,next) {
        const {userId} = req.query,
            {postId} = req.params,
            comment = req.body;
        if(!userId) return res.status(400).json({
            error: "Cannot identify user"
        })
        if(!postId) return res.status(400).json({
            error: "Cannot identify post"
        })
        PostModel.findById(postId, (err, doc:Post) => {
            if(err) return res.status(500).json({error: err.message})

            var newComment = new PostCommentModel(new PostComment(userId, comment.content, null));
            newComment.save(undefined, (err, product) => {
                newComment = product.toObject();
                doc.comments.push(newComment._id) 
                PostModel.updateOne({_id: postId}, doc, (error, raw) => {
                    if(error) return res.status(500).json({error: error.message})
                    PostModel.findById(postId).populate(['creator','comments','likes','shares','files']).exec((err, doc) => {
                        if(err) return res.status(500).json({error: err.message})
                        PostCommentModel.populate(doc, [{
                            path: 'comments.creator',
                            model: 'User'
                        },{
                            path: 'comments.file',
                            model: 'File'
                        }]).then((doc) => {
                            res.status(200).json(doc)
                        })
                        .catch(next)
                    })
                })
            })                    
        })
    }

    static dislikePost(req,res,next) {
        const {userId} = req.query,
            {postId} = req.params;
        if(!userId) return res.status(400).json({
            error: "Cannot identify user"
        })
        if(!postId) return res.status(400).json({
            error: "Cannot identify post"
        })
        PostModel.findById(postId, (err, doc:Post) => {
            if(err) return res.status(500).json({error: err.message})
            if(!doc.likes.includes(userId)) doc.likes = doc.likes.filter(like => like != userId)
            PostModel.updateOne({_id: postId}, doc, (error, raw) => {
                if(error) return res.status(500).json({error: error.message})
                PostModel.findById(postId).populate(['creator','comments','likes','shares','files']).exec((err, doc) => {
                    if(err) return res.status(500).json({error: err.message})
                    PostCommentModel.populate(doc, [{
                        path: 'comments.creator',
                        model: 'User'
                    },{
                        path: 'comments.file',
                        model: 'File'
                    }]).then((doc) => {
                        res.status(200).json(doc)
                    })
                    .catch(next)
                })
            })                
        })  
    }

    static likePost(req,res,next) {
        const {userId} = req.query,
            {postId} = req.params;
        if(!userId) return res.status(400).json({
            error: "Cannot identify user"
        })
        if(!postId) return res.status(400).json({
            error: "Cannot identify post"
        })
        PostModel.findById(postId, (err, doc:Post) => {
            if(err) return res.status(500).json({error: err.message})
            if(!doc.likes.includes(userId)) doc.likes.push(userId);
            PostModel.updateOne({_id: postId}, doc, (error, raw) => {
                if(error) return res.status(500).json({error: error.message})
                PostModel.findById(postId).populate(['creator','comments','likes','shares','files']).exec((err, doc) => {
                    if(err) return res.status(500).json({error: err.message})
                    PostCommentModel.populate(doc, [{
                        path: 'comments.creator',
                        model: 'User'
                    },{
                        path: 'comments.file',
                        model: 'File'
                    }]).then((doc) => {
                        res.status(200).json(doc)
                    })
                    .catch(next)
                })
            })                
        })  
    }

    static fetchNewsFeed(req,res,next) {
        const {userId} = req.query;
        if(!userId) return res.status(400).json({
            error: "Cannot identify user"
        })
        const userManagementService = new ServicesController('user_management_service')
        var url = `/data/users/${userId}`
        userManagementService.api.get(url)
        .then(response => {
            var user = response.data;
            var ids = [user._id];
            if(user.friends && user.friends.length) {
                ids.push(...user.friends)
            }
            PostModel.find({creator: {$in: ids}}).sort({created_at: 'desc'}).populate(['creator','likes','shares','files','comments']).limit(100).exec((err, docs) => {
                if(err) return res.status(500).json({error: err.message})
                PostCommentModel.populate(docs, [{
                    path: 'comments.creator',
                    model: 'User'
                },{
                    path: 'comments.file',
                    model: 'File'
                }]).then((docs) => {
                    // shuffle(docs)
                    res.status(200).json(docs)
                })
                .catch(next)
            })
        })
        .catch(next)
    }

    static async createPost(post: Partial<Post>) {
        post = new Post(post);
        if(!post.validate()) {
            throw new Error(`Invalid post`)
        }
        try {
            var createdPost = await PostModel.create(post);
            const metaData = {
                doc_text: `${post.post_content}`,
                doc_id: createdPost._id,
                doc_collection: "posts",
                doc_db: "soma",
                service: "post_management_service"
            }
            await MetaDataController.createMetaData(metaData)
        } catch (error) {
            return {
                success: false,
                error
            }
        }
        return {
            success: true,
            createdPost
        }
    }

    static async handleCreatePost(req,res,next) {
        const {post} = req.body;
        if(!post) return res.status(400).json({
            error: `Missing payload`
        })
        try {
            var result = await PostController.createPost(post);            
        } catch (error) {
            return res.status(500).json({
                error: error.message
            })
        }
        if(!result.success) return res.status(500).json(result.error);
        return res.status(200).json(result.createdPost)
    }
}

export default PostController