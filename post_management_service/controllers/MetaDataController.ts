import config from "../config"
import axios, { AxiosInstance } from "axios"

var api:AxiosInstance = null

class MetaDataController {
    constructor() {
        api = axios.create({
            baseURL: config.services.search_engine.baseURL,
            timeout: 60000,
            headers: {
                "X-Application-Key": config.services.search_engine.apiKey
            }
        })
    }

    static async createMetaData(metadata) {
        if(!metadata) return;
        metadata.service = metadata.service || "post_management_service";
        await api.post('/metadata', {
            metadata
        })        
    }
}

new MetaDataController

export default MetaDataController