import mongoose from "mongoose"
import autoIncrement from "mongoose-auto-increment"
import moment from "moment"

autoIncrement.initialize(mongoose.connection)

const userSchema = new mongoose.Schema({
    _id: {type: Number, required:true},
    name: {type: String, default: null},
    birth_date: {type: Date, default: null},
    email: {type: String, default: null},
    phone: {type: String, default: null},
    gender: {type: String, default: 'male'},
    avatar_path: {type: String, default: null},
    cover_path: {type: String, default: null},
    sessions: [{type: Number, ref: 'UserSession'}],
    accounts: [{type: Number, ref: 'Account'}],
    friends: [{type: Number, ref: 'User'}],
    sent_friend_requests: [{type: Number, ref: 'User'}],
    friend_requests: [{type: Number, ref: 'User'}],
    created_at: {type: Date, default: moment.utc(Date.now())},
    updated_at: {type: Date, default: moment.utc(Date.now())},
    deleted_at: {type: Date, default: null}
})

userSchema.plugin(autoIncrement.plugin, { model: 'User', field: '_id', startAt: 1, incrementBy: 1 })

export default mongoose.model('User', userSchema, 'users')