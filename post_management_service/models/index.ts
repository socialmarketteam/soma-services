import {PostModel} from "./Post"
import User from "./User"
import {FileModel} from "./File"
import {PostCommentModel} from "./PostComments"

export default {
    files: FileModel,
    posts: PostModel,
    users: User,
    postComments: PostCommentModel
}