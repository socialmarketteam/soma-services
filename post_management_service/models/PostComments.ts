import mongoose, { mongo } from "mongoose"
import autoIncrement from "mongoose-auto-increment"
import moment from "moment"

autoIncrement.initialize(mongoose.connection);

const postCommentSchema = new mongoose.Schema({
    _id: {type: Number, required: true},
    creator: {type: Number, required: true, ref: 'User'},
    content: {type: String, required: true},
    file: {type: Number, ref: 'File'},
    edited_at: {type: Date, default: null},
    created_at: {type: Date, default: moment.utc().toDate()},
    updated_at: {type: Date, default: moment.utc().toDate()},
    deleted_at: {type: Date, default: null}
})

postCommentSchema.plugin(autoIncrement.plugin, {model: 'PostComment', field: '_id', startAt: 1, incrementBy: 1})

export const PostCommentModel = mongoose.model('PostComment', postCommentSchema, 'post_comments')

export default class PostComment {
    public _id: Number;
    public creator: Number;
    public content: String;
    public file: Number;
    public edited_at: Date;
    public created_at: Date;
    public updated_at: Date;
    public deleted_at: Date;

    constructor(creator, content, file) {
        this.creator=creator;
        this.content=content;
        this.file=file;
    }
}