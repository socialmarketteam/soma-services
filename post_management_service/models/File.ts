import mongoose from "mongoose"
import autoIncrement from "mongoose-auto-increment"
import moment from "moment"

autoIncrement.initialize(mongoose.connection);

const fileSchema = new mongoose.Schema({
    _id: {type: Number, required: true},
    type: {type: String, required: true},
    file_name: {type: String, required: true},
    actual_name: {type: String, required: true},
    url: {type: String, required: true},
    created_at: {type: Date, default: moment.utc().toDate()},
    updated_at: {type: Date, default: moment.utc().toDate()},
    deleted_at: {type: Date, default: null}
})

fileSchema.plugin(autoIncrement.plugin, {model: 'File', field: '_id', startAt: 1, incrementBy: 1});

export const FileModel = mongoose.model('File', fileSchema, 'files')