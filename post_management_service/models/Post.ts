import mongoose from "mongoose"
import autoIncrement from "mongoose-auto-increment"
import moment from "moment"

autoIncrement.initialize(mongoose.connection);

const postSchema = new mongoose.Schema({
    _id: {type: Number, required: true},
    creator: {type: Number, ref: 'User'},
    post_content: {type: String, required: true},
    files: [{type: Number, ref: 'File'}],
    likes: [{type: Number, ref: 'User'}],
    ref: {type: Number, ref: 'Post'},
    shares: [{type: Number, ref: 'Post'}],
    comments: [{type: Number, ref: 'PostComment'}],
    edited_at: {type: Date, default: null},
    created_at: {type: Date, default: moment.utc().toDate()},
    updated_at: {type: Date, default: moment.utc().toDate()},
    deleted_at: {type: Date, default: null}
})

postSchema.plugin(autoIncrement.plugin, {model: 'Post', field: '_id', startAt: 1, incrementBy: 1});

export const PostModel = mongoose.model('Post', postSchema, 'posts')

export default class Post {
    public _id: number;
    public creator_id: number;
    public post_content: string;
    public files: number[];
    public likes: number[];
    public ref: number[];
    public shares: number[];
    public comments: number[];
    public edited_at: Date;
    public created_at: Date;
    public updated_at: Date;
    public deleted_at: Date;

    constructor(post: Partial<Post>) {
        this.post_content = post.post_content || null;
        this.creator_id = post.creator_id || null;
        this._id = post._id || null;
        this.edited_at = post.edited_at || null;
    }

    validate():boolean {
        if(!this.post_content || !this.creator_id) return false;
        return true
    }
}